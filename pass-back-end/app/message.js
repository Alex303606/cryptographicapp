const express = require('express');
const router = express.Router();
const Caesar = require('caesar-salad').Caesar;

router.post('/encode', (req, res) => {
	const encodedMessage = Caesar.Cipher(req.body.password).crypt(req.body.decodedMessage);
	res.send(encodedMessage);
});

router.post('/decode', (req, res) => {
	const decodedMessage = Caesar.Decipher(req.body.password).crypt(req.body.encodedMessage);
	res.send(decodedMessage);
});

module.exports = router;
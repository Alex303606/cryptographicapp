const express = require('express');
const cors = require('cors');
const message = require('./app/message');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());

app.use('/', message);

app.listen(port, () => {
	console.log(`Server started on ${port} port!`);
});


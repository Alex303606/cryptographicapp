import React, {Component} from 'react';
import './App.css';
import {connect} from 'react-redux';
import {decodeMessage, encodeMessage, inputChangeHandler} from "./store/actions/actions";

class App extends Component {
	
	encodeMessageHandler = () => {
		this.props.onEncodeMessage();
	};
	
	decodeMessageHandler = () => {
		this.props.onDecodeMessage();
	};
	
	render() {
		return (
			<div className="App">
				<div className="row">
					<label htmlFor="decoded">Сообщение</label>
					<textarea
						onChange={(e) => this.props.inputChangeHandler(e)}
						value={this.props.decodedMessage}
						name="decodedMessage"
						id="decoded"/>
				</div>
				<div className="row pass">
					<label htmlFor="pass">Password</label>
					<input
						value={this.props.password}
						onChange={(e) => this.props.inputChangeHandler(e)}
						name="password"
						placeholder="example: abc-123 or 123-abc or 123 or abc"
						type="text"
						id="pass"/>
					<button onClick={this.encodeMessageHandler}>Закодировать</button>
					<button onClick={this.decodeMessageHandler}>Раскодировать</button>
				</div>
				<div className="row">
					<label htmlFor="encoded">Закодированное сообщение</label>
					<textarea
						onChange={(e) => this.props.inputChangeHandler(e)}
						value={this.props.encodedMessage}
						name="encodedMessage"
						id="encoded"/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		encodedMessage: state.encodedMessage,
		decodedMessage: state.decodedMessage,
		password: state.password
	}
};
const mapDispatchToProps = dispatch => {
	return {
		onEncodeMessage: () => dispatch(encodeMessage()),
		onDecodeMessage: () => dispatch(decodeMessage()),
		inputChangeHandler: (e) => dispatch(inputChangeHandler(e))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

import {INPUT_CHANGE, SEND_TO_DECODE, SEND_TO_ENCODE} from "../actions/actionsTypes";

const initialState = {
	encodedMessage: '',
	decodedMessage: '',
	password: ''
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case INPUT_CHANGE:
			action.event.persist();
			return {...state, [action.event.target.name]: action.event.target.value};
		case SEND_TO_DECODE:
			return {...state, encodedMessage: action.data};
		case SEND_TO_ENCODE:
			console.log(action.data);
			return {...state, decodedMessage: action.data};
		default:
			return state;
	}
};

export default reducer;
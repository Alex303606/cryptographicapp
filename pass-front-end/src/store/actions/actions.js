import axios from '../../axios-api';
import {INPUT_CHANGE, SEND_TO_DECODE, SEND_TO_ENCODE} from "./actionsTypes";

const CheckSpaces = (str) => {
	return str.trim() !== '';
};

export const encodeMessageSuccess = data => {
	return {type: SEND_TO_DECODE, data};
};

export const encodeMessage = () => {
	return (dispatch, getState) => {
		if(getState().password.length !== 0 && CheckSpaces(getState().password)) {
			axios.post('/encode', getState()).then(
				response => {
					dispatch(encodeMessageSuccess(response.data))
				}
			);
		} else {
			alert('Напишите верный пароль!');
		}
	}
};

export const decodeMessageSuccess = data => {
	return {type: SEND_TO_ENCODE, data};
};

export const decodeMessage = () => {
	return (dispatch, getState) => {
		if(getState().password.length !== 0 && CheckSpaces(getState().password)) {
			axios.post('/decode', getState()).then(
				response => {
					dispatch(decodeMessageSuccess(response.data))
				}
			);
		}
	}
};

export const inputChangeHandler = event => {
	return {type: INPUT_CHANGE, event};
};